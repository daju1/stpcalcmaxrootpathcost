// StpCalcMaxRootPathCost.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include "NetFileParser.h"
#include "NetTree.h"


int main(int argc, char **argv)
{
    // File name of input file with bridged LAN description
    const char * fn = "../net.net";

    if (argc > 1) {
        fn = argv[1];
    }

    // Parsing of input file with bridged LAN description
    NetFileParser * parser = new NetFileParser(fn);
    if (!parser->isOk())
    {
        printf("Error of parsing bridged LAN description file \"%s\"\n", fn);
        delete parser;

        return -1;
    }

    /*
        The Bridge with the highest priority Bridge Identifier is the Root (for convenience of calculation, this is the
    identifier with the lowest numerical value). Every Bridge Port in the Bridged LAN has a Root Path Cost
    associated with it. This is the sum of the Path Costs for each Bridge Port receiving frames forwarded from
    the Root on the least cost path to the Bridge. The Designated Port for each LAN is the Bridge Port for which
    the value of the Root Path Cost is the lowest: if two or more Ports have the same value of Root Path Cost,
    then first the Bridge Identifier of their Bridges and then their Port Identifiers are used as tie- breakers. Thus,
    a single Bridge Port is selected as the Designated Port for each LAN, the same computation selects the Root
    Port of a Bridge from among the Bridge�s own Ports, and the active topology of the Bridged LAN is completely
    determined.
    */

    // Get Name of root bridge 
    //this is the identifier with the lowest numerical value
    std::string rootBridgeName = parser->getRootBridgeName();

    // get map of LANs from parser
    LANs lans = parser->getLANs();

    // get constant reference of root bridge
    const Bridge& rootBridge = parser->getBridge(rootBridgeName);

    // build bridge LAN tree
    int root_path_cost = 0;
    BridgeTree * root = new BridgeTree(rootBridgeName, rootBridgeName, rootBridge, NULL, lans, parser, root_path_cost);

#ifdef _DEBUG
    // Debug print the structure of the built tree
    int start_level = 0;
    root->walk(start_level);
#endif

    // Get resulting value of Root Path Cost 
    int maxRootPathCost = parser->getMaxRootPathCost();
    printf("Max Root Path Cost = %d\n", maxRootPathCost);

    // free 
    delete root;
    delete parser;

    return 0;
}

