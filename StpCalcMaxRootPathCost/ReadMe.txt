========================================================================
     StpCalcMaxRootPathCost Project Overview
========================================================================

StpCalcMaxRootPathCost is a program to calculate the maximum value of 
Root Path Cost in an IEEE 802.1d bridged LAN. The configuration parameters 
of bridges and the structure of the network are provided in the input data 
for the program.

The standard IEEE 802.1d-1998 can be found in the Internet. 
The description of Spanning Tree Algorithm and Protocol and the definitions 
of bridge, LAN, bridge port, etc. could be found in this standard.

The program read input data from file. The name of file is specified in command line.
If the name of the file is not provided, the program gets input data from the standard input.
Example of input data (for the bridged LAN shown in Figure 8-1, 8-2 of the standard):

[BRIDGE 1]
BridgeId = 42

[BRIDGE 1.Port 01]
PathCost = 10
ConnectedTo = LAN A

[BRIDGE 1.Port 02]
PathCost = 10
ConnectedTo = LAN B


[BRIDGE 2]
BridgeId = 97

[BRIDGE 2.Port 01]
PathCost = 05
ConnectedTo = LAN C

[BRIDGE 2. Port 02]
PathCost = 10
ConnectedTo = LAN A

[BRIDGE 2. Port 03]
PathCost = 05
ConnectedTo = LAN D


The program prints the output data to the standard output (console). 
Example of program output:

Max Root Path Cost = 10


This file contains a summary of what you will find in each of the files that
make up your StpCalcMaxRootPathCost application.


StpCalcMaxRootPathCost.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

StpCalcMaxRootPathCost.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).


NetFileParser.h, NetFileParser.cpp
    This is realisation of bridged LAN description input file parser. This paser read 
    data from the input file and save information about the bridged LAN in its inner structure.

NetTree.h
    This is a realisation of bridged LAN tree structure, which builds with logic described in the 
    above mentioned standard. While filling bridged LAN tree root_path_cost of each bridge is calculed.

StpCalcMaxRootPathCost.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
