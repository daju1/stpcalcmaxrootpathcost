#pragma once
#include "NetFileParser.h"
#include <list>

typedef struct BridgeTreeNode
{
    const Bridge& bridge;

    typedef  BridgeTreeNode Child;

    BridgeTreeNode * parent;
    std::list<Child> children;

    BridgeTreeNode(std::string rootBridgeName, 
        std::string bridgeName, 
        const Bridge& b, 
        BridgeTreeNode * a_parent, 
        LANs& lans, 
        NetFileParser * parser, 
        unsigned int cost)
        : bridge(b)
        , parent(a_parent)
    {
        bridge.root_path_cost = cost;
        printlog("bridgeName=%s b.BridgeId=%s bridge.root_path_cost=%d\n", bridgeName.c_str(), b.BridgeId.c_str(), bridge.root_path_cost);

        auto it_bridge_ports = bridge.ports.begin();
        auto end_bridge_ports = bridge.ports.end();

        for (; it_bridge_ports != end_bridge_ports; ++it_bridge_ports)
        {
            std::string port_number = it_bridge_ports->first;
            Port port = it_bridge_ports->second;
            LAN_name lan_name = port.ConnectedTo;
            std::string path_cost = port.PathCost;

            LAN& lan = lans.at(lan_name);
            LAN::LinkBeetweenLANandBridge& lan_bridge_link_direct = lan.links.at(bridgeName);
            printlog("direct lanName=%s bridgeNumber=%s portNumber=%s cost=%s included_in_tree=%d\n",
                lan_bridge_link_direct.lanName.c_str(),
                lan_bridge_link_direct.bridgeNumber.c_str(),
                lan_bridge_link_direct.portNumber.c_str(),
                lan_bridge_link_direct.cost.c_str(),
                lan_bridge_link_direct.included_in_tree);

            auto it_lan_link = lan.links.begin();
            auto end_lan_link = lan.links.end();

            for (; it_lan_link != end_lan_link; ++it_lan_link)
            {
                std::string child_bridge_name = it_lan_link->first;

                if (child_bridge_name == bridgeName) {
                    continue;
                }

                if (child_bridge_name == rootBridgeName) {
                    continue;
                }
                LAN::LinkBeetweenLANandBridge& lan_bridge_link_back = it_lan_link->second;

                printlog("back lanName=%s bridgeNumber=%s portNumber=%s cost=%s included_in_tree=%d\n",
                    lan_bridge_link_back.lanName.c_str(),
                    lan_bridge_link_back.bridgeNumber.c_str(),
                    lan_bridge_link_back.portNumber.c_str(),
                    lan_bridge_link_back.cost.c_str(),
                    lan_bridge_link_back.included_in_tree);

                /*Every Bridge Port in the Bridged LAN has a Root Path Cost
                associated with it. This is the sum of the Path Costs for each Bridge Port receiving frames forwarded from
                the Root on the least cost path to the Bridge.
                */
                const Bridge& child = parser->getBridge(child_bridge_name);
                unsigned int child_cost = atoi(lan_bridge_link_back.cost.c_str());
                unsigned int child_root_path_cost = child.root_path_cost;
                unsigned int new_child_root_path_cost = b.root_path_cost + child_cost;

                if (unsigned int(-1) != child_root_path_cost)
                {
                    printlog("%s %s child_root_path_cost %d\n", child_bridge_name.c_str(), child.BridgeId.c_str(), child_root_path_cost);
                }

                if (new_child_root_path_cost > child_root_path_cost)
                {
                    printlog("new_child_root_path_cost %d > child_root_path_cost %d continue\n", 
                        new_child_root_path_cost, child_root_path_cost);
                    continue;
                }
                it_bridge_ports->second.included_in_tree++;
                lan_bridge_link_direct.included_in_tree++;
                lan_bridge_link_back.included_in_tree++;

                // add child
                children.push_back(Child(rootBridgeName, child_bridge_name, child, this, lans, parser, new_child_root_path_cost));
            }

        }
    } 

    void walk(int level) const
    {
        printlog("level=%d BridgeId=%s root_path_cost=%d\n", 
            level, bridge.BridgeId.c_str(), 
            this->bridge.root_path_cost
        );
        for (auto & n : children) 
            n.walk(level+1);
    }
} BridgeTree;

//template< typename T >

