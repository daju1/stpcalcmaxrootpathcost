#pragma once
#include <map>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <stdarg.h>

static void printlog(const char *fmt, ...)
{
#ifdef _DEBUG
    static char message[8192];

    va_list args;
    va_start(args, fmt);
    vsnprintf_s(message, sizeof message, sizeof message, fmt, args);
    va_end(args);

    printf("%s", message);
#endif
}


typedef struct Section
{
    typedef std::map<std::string, std::string> SectionProperties;

    void ensureKeyIsUnique(const std::string& key)
    {
        if (properties.count(key) != 0)
        {
            throw std::runtime_error(key + " appears twice in port properties");
        }
    }

    void insert(std::pair<std::string, std::string> keyValuePair)
    {
        properties.insert(keyValuePair);
        ParseProperties();
    }
protected:
    SectionProperties properties;
    virtual void ParseProperties() = 0;

} Section;

typedef struct Port : public Section
{
    typedef SectionProperties PortProperties;
    std::string PathCost;
    std::string ConnectedTo;
    mutable int included_in_tree;
    Port()
        : included_in_tree(0)
    {}
private:
    virtual void ParseProperties()
    {
        std::vector<std::string> sectionNames;
        for (auto it = properties.begin(); it != properties.end(); ++it)
        {
            if ("PathCost" == it->first)
            {
                PathCost = it->second;
            }
            else if ("ConnectedTo" == it->first)
            {
                ConnectedTo = it->second;
            }
            else
            {
                throw std::runtime_error("unknown port property " + it->first);
            }
        }
    }
} Port;


typedef struct Bridge : public Section
{
    typedef std::map<std::string, Port> Ports;
    typedef SectionProperties BridgeProperties;
    Ports ports;
    std::string BridgeId;
    mutable unsigned int root_path_cost;
    Bridge() 
        : root_path_cost(-1)
    {}
private:
    virtual void ParseProperties()
    {
        std::vector<std::string> sectionNames;
        for (auto it = properties.begin(); it != properties.end(); ++it)
        {
            if ("BridgeId" == it->first)
            {
                BridgeId = it->second;
            }
            else
            {
                throw std::runtime_error("unknown bridge property " + it->first);
            }
        }
    }
} Bridge;


typedef std::map<std::string, Bridge> Bridges;
typedef std::string LAN_name;
typedef struct LAN
{
    typedef struct LinkBeetweenLANandBridge
    {
        std::string lanName;
        std::string bridgeNumber;
        std::string portNumber;
        std::string cost;
        int included_in_tree;
        LinkBeetweenLANandBridge()
            : included_in_tree(0)
        {}
    } LinkBeetweenLANandBridge;

    typedef std::string nameOfBridge;
    std::map<nameOfBridge, LinkBeetweenLANandBridge> links;
    bool included_in_tree;
    LAN()
        : included_in_tree(false)
    {}
}LAN;
typedef std::map<LAN_name, LAN> LANs;

class NetFileParser
{
public:
    NetFileParser(const std::string& pathname);
    const Bridge& getBridge(const std::string& bridgeName) const
    {
        return mBridges.at(bridgeName);
    }

    LANs getLANs() const;
    std::vector<std::string> getBridges() const;
    std::string getRootBridgeName() const;
    bool isOk() const
    {
        return mBridges.size() > 0;
    }
    unsigned int getMaxRootPathCost() const;

private:
    Bridges parseFile(const std::string& pathname);
    void parseLine(const std::string& line, Bridges& bridges);
    void addSection(const std::string& line, Bridges& bridges);
    void addKeyValuePair(const std::string& line, Bridges& bridges) const;

    std::string mCurrentBridge;
    std::string mCurrentPort;
    const Bridges mBridges;
};