#include "NetFileParser.h"
#include <algorithm>
#include <cctype>
#include <stdexcept>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>


// The function in this namespace will go into a custom string util library
namespace stringUtil
{

    // trim from start (in place)
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
            return !std::isspace(ch);
        }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string &s) {
        ltrim(s);
        rtrim(s);
    }

    std::vector<std::string> split(const std::string& string, char delimiter = ' ')
    {
        std::vector<std::string> result;
        std::stringstream stream(string);
        std::string word;
        while (std::getline(stream, word, delimiter))
        {
            trim(word);
            result.push_back(word);
        }
        return result;
    }
}

namespace
{
    bool isComment(const std::string& line)
    {
        return line[0] == ';';
    }

    std::string extractSectionName(const std::string& line)
    {
        return std::string(line.begin() + 1, line.end() - 1);
    }

    bool isSectionHeading(const std::string& line)
    {
        if (line[0] != '[' || line[line.size() - 1] != ']')
        {
            return false;
        }
        const std::string sectionName = extractSectionName(line);
        return sectionName.size() > 0;
    }

    bool isKeyValuePair(const std::string& line)
    {
        // Assume we have already checked if it's a comment or section header.
        return std::count(line.begin(), line.end(), '=') == 1;
    }

    void ensureSectionIsUnique(const std::string& bridgeName, const Bridges& sections)
    {
        if (sections.count(bridgeName) != 0)
        {
            throw std::runtime_error(bridgeName + " appears twice in config file");
        }
    }

    void ensureSectionIsUnique(const std::string& portName, const Bridge::Ports& sections)
    {
        if (sections.count(portName) != 0)
        {
            throw std::runtime_error(portName + " appears twice in config file");
        }
    }

    void ensureCurrentSection(const std::string& line, const std::string& currentSection)
    {
        if (currentSection.empty())
        {
            throw std::runtime_error(line + " does not occur within a section");
        }
    }

    std::pair<std::string, std::string> parseKeyValuePair(const std::string& line)
    {
        std::vector<std::string> pair = stringUtil::split(line, '=');
        return std::pair <std::string, std::string>(pair[0], pair[1]);
    }
}

NetFileParser::NetFileParser(const std::string& pathname) :
    mCurrentBridge(""),
    mCurrentPort(""),
    mBridges(parseFile(pathname))
{
}

std::vector<std::string> NetFileParser::getBridges() const
{
    std::vector<std::string> bridgesNames;
    for (auto it = mBridges.begin(); it != mBridges.end(); ++it)
    {
        bridgesNames.push_back(it->first);
    }
    return bridgesNames;
}

unsigned int NetFileParser::getMaxRootPathCost() const
{
    unsigned int maxRootPathCost = 0;
    for (auto it = mBridges.begin(); it != mBridges.end(); ++it)
    {
        if (maxRootPathCost < it->second.root_path_cost)
            maxRootPathCost = it->second.root_path_cost;
    }
    return maxRootPathCost;
}

LANs NetFileParser::getLANs() const
{
    LANs lans;
    for (auto it_b = mBridges.begin(); it_b != mBridges.end(); ++it_b)
    {
        std::string bridgeName = it_b->first;

        auto it_p = it_b->second.ports.begin();
        auto end_p = it_b->second.ports.end();

        for (; it_p != end_p; ++it_p)
        {
            LAN_name lan_name = it_p->second.ConnectedTo;
            auto found = lans.find(lan_name);
            if (found == lans.end())
                lans.insert(std::pair<LAN_name, LAN> (lan_name, LAN()));

            std::string bridge_name = it_b->first;

            LAN::LinkBeetweenLANandBridge lan_bridge_link;
            lan_bridge_link.lanName = lan_name;
            lan_bridge_link.bridgeNumber = bridgeName;
            lan_bridge_link.portNumber = it_p->first;
            lan_bridge_link.cost = it_p->second.PathCost;

            lans.at(lan_name).links.insert(std::pair<LAN::nameOfBridge, LAN::LinkBeetweenLANandBridge> (bridge_name, lan_bridge_link));
        }
    }
    return lans;
}



std::string NetFileParser::getRootBridgeName() const
{
    /*
    The Bridge with the highest priority Bridge Identifier is the Root
    (for convenience of calculation, this is the
    identifier with the lowest numerical value).
    */
    std::string rootBridgeName;

    std::string minBridgeId;
    std::string bridgeId;

    for (auto it = mBridges.begin(); it != mBridges.end(); ++it)
    {
        bridgeId = it->second.BridgeId;
        if (it == mBridges.begin() || bridgeId < minBridgeId) {
            minBridgeId = bridgeId;
            rootBridgeName = it->first;
        }
    }
    return rootBridgeName;
}

Bridges NetFileParser::parseFile(const std::string& pathname)
{
    Bridges bridges;
    std::ifstream input(pathname);
    std::string line;
    while (std::getline(input, line))
    {
        parseLine(line, bridges);
    }
    return bridges;
}

void NetFileParser::parseLine(const std::string& line, Bridges& bridges)
{
    if (0 == line.size())
    {
        return;
    }
    else if (isComment(line))
    {
        return;
    }
    else if (isSectionHeading(line))
    {
        addSection(line, bridges);
        return;
    }
    else if (isKeyValuePair(line))
    {
        addKeyValuePair(line, bridges);
        return;
    }

    throw std::runtime_error("unknown rule for parse line " + line);
}

void NetFileParser::addSection(const std::string& line, Bridges& bridges)
{
    const std::string sectionName = extractSectionName(line);

    std::cmatch m_port;
    std::regex re_port("BRIDGE ([0-9]+)\\.[ ]?Port ([0-9]+)");
    if (std::regex_match(sectionName.c_str(), m_port, re_port))
    {
        if (3 == m_port.size())
        {    
            std::string n_bridge = m_port[1].str();
            std::string n_port = m_port[2].str();
            ensureSectionIsUnique(n_port, bridges[mCurrentBridge].ports);

            bridges[mCurrentBridge].ports.insert(std::pair<std::string, Port>(n_port, Port()));
            mCurrentPort = n_port;
            return;
        }
    }

    std::cmatch m_bridge;
    std::regex re_bridge("BRIDGE ([0-9]+)");
    if (std::regex_match(sectionName.c_str(), m_bridge, re_bridge))
    {
        if (2 == m_bridge.size())
        {
            std::string n_bridge = m_bridge[1].str();
            ensureSectionIsUnique(n_bridge, bridges);

            bridges.insert(std::pair<std::string, Bridge>(n_bridge, Bridge()));
            mCurrentBridge = n_bridge;
            mCurrentPort = "";
            return;
        }
    }

    throw std::runtime_error("unknown rule for parse section name " + sectionName);
}

void NetFileParser::addKeyValuePair(const std::string& line, Bridges& bridges) const
{
    ensureCurrentSection(line, mCurrentBridge);
    const auto keyValuePair = parseKeyValuePair(line);

    if (mCurrentPort.empty())
    {
        bridges.at(mCurrentBridge).ensureKeyIsUnique(keyValuePair.first);
        bridges.at(mCurrentBridge).insert(keyValuePair);
    }
    else
    {
        Bridge::Ports& ports = bridges.at(mCurrentBridge).ports;
        ports.at(mCurrentPort).ensureKeyIsUnique(keyValuePair.first);
        ports.at(mCurrentPort).insert(keyValuePair);
    }
}
